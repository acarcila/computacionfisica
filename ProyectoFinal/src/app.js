// Cargar modulos y crear nueva aplicacion
var express = require("express");
var app = express();
var cors = require("cors");
var bodyParser = require("body-parser");
var bodyParser = require("body-parser");

app.use(bodyParser.json()); // soporte para bodies codificados en jsonsupport
app.use(bodyParser.urlencoded({ extended: true })); // soporte para bodies codificados
app.use(cors());

const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
var port = new SerialPort('COM6', { baudRate: 9600 });
const parser = port.pipe(new Readline({ delimiter: '\n' }));

// Read the port data
port.on("open", () => {
    console.log('serial port open');
});
parser.on('data', data => {
    console.log('got word from arduino:', data);
});

//Ejemplo: GET http://localhost:8080
app.get("/", function (req, res, next) {
    port.write('a', (err) => {
        if (err) {
            return console.log('Error on write: ', err.message);
        }
        console.log('message written');
        res.json('a');
    });
});

var server = app.listen(8080, function () {
    console.log("Server is running..");
  });