#include <Servo.h>
Servo servo;

bool bandera = false;

void setup()
{
  servo.attach(3);
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  attachInterrupt(0, cambiarEstado, RISING);
}

void loop()
{
  if(bandera){
    servo.write(120);
    digitalWrite(13, HIGH);
  }else{
    servo.write(0);
    digitalWrite(13, LOW);
  }

  if(Serial.read() == 'a'){
    bandera = !bandera;
  }
  
  delay(100);
}

void cambiarEstado(){
  bandera = !bandera;
  Serial.println(bandera);
}
